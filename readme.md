<!-- const - let -->

                                      LET & CONST

* CONST

  * const is immutable varriables, variables which cannot be re-assigned new content.
  * const declarations are block scoped.
  * const declarations can only be accessed within the block it was declared.
  * const cannot be updated or re-declared.
  * Every const declaration therefore, must be initialized at the time of declaration.

  - ex:

        const flow = {
            message : "say Hi",
            times : 4
        }

    * Hoisting of const
      * Like let & var, const declarations are hoisted to the top but are not initialized.

* LET

  * let is preferred for variable declaration now.
  * let is block scoped.
  * let can be updated but not re-declared.
  * This fact makes let a better choice than var. When using let, you don't have to bother if you have used a name for a variable before as a variable exists only within its scope. Also, since a variable cannot be declared more than once within a scope, then the problem discussed earlier that occurs with var does not occur.

        let greeting = "say Hi";
        if (true) {
        let greeting = "say Hello instead";
        console.log(greeting);
        }
        console.log(greeting);

  * Hoisting of let
    * let declarations are hoisted to the top

<!-- Template String-->

                                     TEMPLATE STRING

* TEMPLATE STRING

  * Template literals are string literals allowing embedded expressions.
  * Template literals are enclosed by the back-tick (``) character instead of double or single quotes. Template literals can contain placeholders. These are indicated by the dollar sign and curly braces (${expression}).

    SYNTAX:

    `string text`
    `string text line 1 string text line 2`

    `string text ${expression} string text`

    tag `string text ${expression} string text`

<!-- Promise -->

                                    PROMISE

* PROMISE

  * The Promise object represents the eventual completion (or failure) of an asynchronous operation, and its resulting value.

    SYNTAX:

        var promise = new Promise(callback);

        * callback = function(resolve, reject)

  * Pending - Fulfilled - Rejected

    * Pending is the initial promise state. The operation represented by the promise has not yet been fulfilled or rejected.

      * var promise = new Promise(function(resolve, reject){
        });

      console.log(promise);


    * Fulfilled is a state of a Promise. It means that the promise has been resolved and now has its resolved value (using the internal resolve function). The operation represented by the promise has been completed successfully.
        * var promise = new Promise(function(resolve, reject){

             resolve();
             });

            console.log(promise);

    * Rejected means that the promise has been rejected and now has its rejected reason (using the internal reject function). The operation represented by the promise failed to obtain a value and thus has a reason for failing to do so (typically an error code or error object, but it can be anything).
     * var promise = new Promise(function(resolve, reject){
    reject();
        });

        promise.catch(function(){

        // Something


        console.log(promise);

<!-- Destructuring Assignments -->

                                DESTRUCTURING ASSIGNMENTS

* DESTRUCTURING ASSIGNMENTS

  * The destructuring assignment syntax is a JavaScript expression that makes it possible to unpack values from arrays, or properties from objects, into distinct variables.

         let newCommer = {
            name: 'ngotantai',
            sex: 'male',
            relationship: 'open'
        }
        let {
            name: n,
            sex: s,
            relationship: r
        } = newCommer;
        document.write("name : " + n);

<!-- map and Foreach -->

                                     MAP AND FOREACH

* MAP AND FOREACH

  * Map

  - The map() method creates a new array with the results of calling a function for every array element.

  - The map() method calls the provided function once for each element in an array, in order.
  - map() does not execute the function for array elements without values.

        SYNTAX:
            array.map(function(currentValue, index, arr), thisValue)

  * For each

    * The forEach() method calls a provided function once for each element in an array, in order.
    * forEach() does not execute the function for array elements without values.

      SYNTAX :
      array.forEach(function(currentValue, index, arr), thisValue)

<!-- Async & Await -->

                                ASYNC -- AWAIT

* async : async function someName(){...}
* await : var result = await someAsyncCall ()
